﻿using System.Runtime.InteropServices;
using System.Web;
using ContractorTimesheet.Models;
using Microsoft.Office.Interop.Excel;

namespace ContractorTimesheet.Service
{
    public static class TimesheetGenerator
    {
        private const string ExcelExtension = ".xlsx";
        private const string FolderWithExcelFiles = "~/ExcelFiles";

        public static string Generate(GenerateTimesheetParameters generateTimesheetParameters)
        {
            var excelApplication = new Application();

            object missingValue = System.Reflection.Missing.Value;

            var workBook = excelApplication.Workbooks.Add(missingValue);
            Worksheet workSheet = (Worksheet) workBook.Worksheets.Item[1];

            BuildExcelFile(generateTimesheetParameters, workSheet);

            var excelFileName = $"{generateTimesheetParameters.FullName}_{generateTimesheetParameters.JobTitle}_{generateTimesheetParameters.ClientName}{ExcelExtension}";

            SaveTimesheet(excelFileName, workBook);

            QuitAndReleaseExcel(workBook, missingValue, excelApplication, workSheet);

            return excelFileName;
        }

        private static void BuildExcelFile(GenerateTimesheetParameters generateTimesheetParameters, Worksheet workSheet)
        {
            BuildContractorAndClientInformation(generateTimesheetParameters, workSheet);

            BuildTimeSheetHeaders(workSheet);

            BuildDateRanges(generateTimesheetParameters, workSheet);

            workSheet.Columns.AutoFit();
        }

        private static void BuildDateRanges(GenerateTimesheetParameters generateTimesheetParameters, Worksheet workSheet)
        {
            var dateRanges = DateRangesCalculator.GetDateRangesForContractor(generateTimesheetParameters);
            var dateRangeCount = 0;

            foreach (var dateRange in dateRanges)
            {
                workSheet.Cells[6 + dateRangeCount, 1] = $"{dateRange.StartDate.ToShortDateString()}-{dateRange.EndDate.ToShortDateString()}";
                dateRangeCount += 1;
            }
        }

        private static void SaveTimesheet(string excelFileName, Workbook workBook)
        {
            var path = HttpContext.Current.Server.MapPath(FolderWithExcelFiles);

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            var completePath = GetCompletePathForTimesheet(excelFileName);

            workBook.SaveAs(completePath);
        }

        private static void QuitAndReleaseExcel(Workbook workBook, object missingValue, Application excelApplication, Worksheet workSheet)
        {
            workBook.Close(true, missingValue, missingValue);
            excelApplication.Quit();

            Marshal.ReleaseComObject(workSheet);
            Marshal.ReleaseComObject(workBook);
            Marshal.ReleaseComObject(excelApplication);
        }

        private static string GetCompletePathForTimesheet(string excelFileName)
        {
            var fullPath = $"{FolderWithExcelFiles}/{excelFileName}";
            string completePath = HttpContext.Current.Server.MapPath(fullPath);

            if (System.IO.File.Exists(completePath))
            {
                System.IO.File.Delete(completePath);
            }
            return completePath;
        }

        private static void BuildTimeSheetHeaders(Worksheet workSheet)
        {
            workSheet.Cells[5, 1] = "Date range";
            workSheet.Cells[5, 2] = "Project";
            workSheet.Cells[5, 3] = "Task reference and description";
            workSheet.Cells[5, 4] = "Normal hours worked";
            workSheet.Cells[5, 5] = "Overtime hours worked";
            workSheet.Cells[5, 6] = "Hours on leave/sick";
            workSheet.Cells[5, 7] = "Hours on call";
            workSheet.Cells[5, 8] = "Remarks/Comments";
        }

        private static void BuildContractorAndClientInformation(GenerateTimesheetParameters generateTimesheetParameters, Worksheet workSheet)
        {
            workSheet.Cells[1, 1] = "Full name:";
            workSheet.Cells[1, 2] = generateTimesheetParameters.FullName;
            workSheet.Cells[2, 1] = "Job title:";
            workSheet.Cells[2, 2] = generateTimesheetParameters.JobTitle;
            workSheet.Cells[3, 1] = "Client name:";
            workSheet.Cells[3, 2] = generateTimesheetParameters.ClientName;
            workSheet.Cells[4, 1] = string.Empty;
            workSheet.Cells[4, 2] = string.Empty;
        }
    }
}