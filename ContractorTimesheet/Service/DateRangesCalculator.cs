﻿using System;
using System.Collections.Generic;
using ContractorTimesheet.Common;
using ContractorTimesheet.Models;

namespace ContractorTimesheet.Service
{
    public static class DateRangesCalculator
    {
        public static List<DateRange> GetDateRangesForContractor(GenerateTimesheetParameters generateTimesheetParameters)
        {
            var dateRanges = new List<DateRange>();

            var startDate = generateTimesheetParameters.PlacementStartDate;
            var endDate = generateTimesheetParameters.PlacementEndDate;

            switch (generateTimesheetParameters.PlacementType)
            {
                case (int)Enums.MaterialType.Weekly:
                    GetDateRangesForWeeklyPlacementType(startDate, endDate, dateRanges);
                    break;
                case (int)Enums.MaterialType.Monthly:
                    GetDateRangesForMonthlyPlacementType(startDate, endDate, dateRanges);
                    break;
            }

            return dateRanges;
        }

        private static void GetDateRangesForMonthlyPlacementType(DateTime startDate, DateTime placementEndDate, List<DateRange> dateRanges)
        {
            var endOfFirstMonth = startDate.AddDays(-startDate.Day).AddMonths(1);

            AddFirstDateRange(startDate, placementEndDate, dateRanges, endOfFirstMonth);

            for (var currentDay = endOfFirstMonth.AddDays(1); currentDay <= placementEndDate; currentDay = currentDay.AddMonths(1))
            {
                var lastDayOfMonth = DateTime.DaysInMonth(currentDay.Year, currentDay.Month);
                var endOfMonthDate =  new DateTime(currentDay.Year, currentDay.Month, lastDayOfMonth);

                AddDateRange(placementEndDate, dateRanges, currentDay, endOfMonthDate);
            }
        }

        private static void AddFirstDateRange(DateTime startDate, DateTime placementEndDate, List<DateRange> dateRanges, DateTime endOfFirstMonth)
        {
            var dateRange = new DateRange()
            {
                StartDate = startDate,
                EndDate = placementEndDate < endOfFirstMonth ? placementEndDate : endOfFirstMonth
            };

            dateRanges.Add(dateRange);
        }

        private static void GetDateRangesForWeeklyPlacementType(DateTime startDate, DateTime endDate, List<DateRange> dateRanges)
        {
            var weekLength = GetAdjustedWeekLength(startDate, endDate);

            for (var currentDay = startDate; currentDay <= endDate; currentDay = currentDay.AddDays(weekLength + 1))
            {
                var weekLengthAfterCurrentDay = currentDay.AddDays(weekLength);
                AddDateRange(endDate, dateRanges, currentDay, weekLengthAfterCurrentDay);
            }
        }

        private static int GetAdjustedWeekLength(DateTime startDate, DateTime endDate)
        {
            var weekLength = 6;
            var totalDaysInRange = (endDate - startDate).Days + 1;

            if (totalDaysInRange % 7 != 0)
            {
                var wholeWeeksInsideRange = totalDaysInRange / 7;
                weekLength = totalDaysInRange / (wholeWeeksInsideRange + 1);
            }

            return weekLength;
        }

        private static void AddDateRange(DateTime endDate, List<DateRange> dateRanges, DateTime currentDay, DateTime currentUpperBoundary)
        {
            var dateRange = new DateRange()
            {
                StartDate = currentDay,
                EndDate = currentUpperBoundary > endDate ? endDate : currentUpperBoundary
            };

            dateRanges.Add(dateRange);
        }
    }
}