﻿using System;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Razor.Generator;
using ContractorTimesheet.Models;
using Microsoft.Office.Interop.Excel;

namespace ContractorTimesheet.Common
{
    public class Enums
    {
        public enum MaterialType
        {
            Weekly = 1,
            Monthly = 2
        };
    }
}