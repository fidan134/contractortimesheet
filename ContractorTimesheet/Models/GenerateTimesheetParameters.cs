﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractorTimesheet.Models
{
    public class GenerateTimesheetParameters
    {
        public string FullName { get; set; }

        public string JobTitle { get; set; }

        public string ClientName { get; set; }

        public int PlacementType { get; set; }

        public DateTime PlacementStartDate { get; set; }

        public DateTime PlacementEndDate { get; set; }

    }
}