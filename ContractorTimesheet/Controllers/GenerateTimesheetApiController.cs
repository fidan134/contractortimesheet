﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ContractorTimesheet.Models;
using ContractorTimesheet.Service;

namespace ContractorTimesheet.Controllers
{
    public class GenerateTimesheetApiController : ApiController
    {
        // GET: api/GenerateTimesheet
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GenerateTimesheet/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/GenerateTimesheet
        public string Post([FromBody]GenerateTimesheetParameters generateTimesheetParameters)
        {
            var submittedParameters = new GenerateTimesheetParameters
            {
                FullName = generateTimesheetParameters.FullName,
                JobTitle = generateTimesheetParameters.JobTitle,
                ClientName = generateTimesheetParameters.ClientName,
                PlacementType = generateTimesheetParameters.PlacementType,
                PlacementStartDate = generateTimesheetParameters.PlacementStartDate,
                PlacementEndDate = generateTimesheetParameters.PlacementEndDate
            };

            return TimesheetGenerator.Generate(submittedParameters);
        }

        // PUT: api/GenerateTimesheet/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GenerateTimesheet/5
        public void Delete(int id)
        {
        }
    }
}
